import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { AboutPage } from '../pages/about/about';
import { ActorsPage } from '../pages/actors/actors';
import { EditActorPage } from '../pages/edit-actor/edit-actor';
import { ShowActorPage } from '../pages/show-actor/show-actor';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ActorsProvider } from '../providers/actors/actors';
import { FilmsProvider } from '../providers/films/films';

import { ComponentsModule } from '../components/components.module';
import {FilmsPage} from "../pages/films/films";
import {EditFilmPage} from "../pages/edit-film/edit-film";
import {ShowFilmPage} from "../pages/show-film/show-film";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ActorsPage,
    HomePage,
    TabsPage,
    EditActorPage,
    ShowActorPage,
    FilmsPage,
    EditFilmPage,
    ShowFilmPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ActorsPage,
    HomePage,
    TabsPage,
    EditActorPage,
    ShowActorPage,
    FilmsPage,
    EditFilmPage,
    ShowFilmPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ActorsProvider,
    FilmsProvider
  ]
})
export class AppModule {}
