export class Category {
  categoryId: number = -1;
  name: string = "All";

  constructor(categoryId: number, name: string) {
    this.categoryId = categoryId;
    this.name = name;
  }
}
