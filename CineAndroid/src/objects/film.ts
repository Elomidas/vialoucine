export class Film {

  filmId: number;
  title: string;
  description: string;

  constructor(filmId: number, title: string, description: string) {
    this.filmId = filmId;
    this.title = title;
    this.description = description;
  }
}
