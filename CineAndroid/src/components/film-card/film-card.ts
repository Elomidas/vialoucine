import {Component, Input} from '@angular/core';
import {Film} from "../../objects/film";
import {NavController} from "ionic-angular";
import {ShowFilmPage} from "../../pages/show-film/show-film";
import {EditFilmPage} from "../../pages/edit-film/edit-film";

/**
 * Generated class for the FilmCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'film-card',
  templateUrl: 'film-card.html'
})
export class FilmCardComponent {

  @Input()
  film: Film;

  constructor(
    public navCtrl: NavController
  ) {
  }

  showPage() {
    this.navCtrl.push(ShowFilmPage, {film: this.film});
  }

  editPage() {
    this.navCtrl.push(EditFilmPage, {edit: true, film: this.film});
  }

}
