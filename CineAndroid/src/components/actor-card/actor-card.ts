import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Actor } from "../../objects/actor";
import { EditActorPage } from "../../pages/edit-actor/edit-actor";
import { ShowActorPage } from "../../pages/show-actor/show-actor";

/**
 * Generated class for the ActorCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'actor-card',
  templateUrl: 'actor-card.html'
})
export class ActorCardComponent {

  @Input() actor: Actor;

  constructor(
      public navCtrl: NavController
  ) {
    //Nothing here
  }

  showPage() {
    this.navCtrl.push(ShowActorPage, {actor: this.actor});
  }

  editPage() {
    this.navCtrl.push(EditActorPage, {edit: true, actor: this.actor});
  }

}
