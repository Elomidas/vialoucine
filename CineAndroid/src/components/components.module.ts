import { NgModule } from '@angular/core';
import { ActorCardComponent } from './actor-card/actor-card';
import { IonicModule } from 'ionic-angular';
import { FilmCardComponent } from './film-card/film-card';

@NgModule({
	declarations: [ActorCardComponent,
    FilmCardComponent],
	imports: [IonicModule.forRoot(ActorCardComponent)],
	exports: [ActorCardComponent,
    FilmCardComponent]
})
export class ComponentsModule {}
