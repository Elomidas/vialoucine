import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ActorsPage } from '../actors/actors';
import { HomePage } from '../home/home';
import {FilmsPage} from "../films/films";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AboutPage;
  tab2Root = HomePage;
  tab3Root = ActorsPage;
  tab4Root = FilmsPage;

  constructor() {
  }
}
