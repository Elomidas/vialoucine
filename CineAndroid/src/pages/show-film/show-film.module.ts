import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowFilmPage } from './show-film';

@NgModule({
  declarations: [
    ShowFilmPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowFilmPage),
  ],
})
export class ShowFilmPageModule {}
