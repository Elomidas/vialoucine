import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FilmsProvider} from "../../providers/films/films";
import {Film} from "../../objects/film";
import {Actor} from "../../objects/actor";

/**
 * Generated class for the ShowFilmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-film',
  templateUrl: 'show-film.html',
})
export class ShowFilmPage {

  film: Film;
  categories: string;
  actors: Actor[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private filmsProvider: FilmsProvider
    ) {
    this.film = navParams.get('film');
    this.actors = [];
    this.categories = "";
  }

  ionViewWillEnter() {
    this.filmsProvider.getFilm(this.film.filmId)
      .subscribe(data => {
        this.film = data;
      });
    this.filmsProvider.getFilmCategories(this.film.filmId)
      .subscribe(data => {
        let cat: string[] = [];
        for(let c of data) {
          cat.push(c.name);
        }
        this.categories = cat.join(', ');
      });
    this.filmsProvider.getFilmActors(this.film.filmId)
      .subscribe(data => {
        this.actors = data;
      });
  }

}
