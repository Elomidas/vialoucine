import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowActorPage } from './show-actor';

@NgModule({
  declarations: [
    ShowActorPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowActorPage),
  ],
})
export class ShowActorPageModule {}
