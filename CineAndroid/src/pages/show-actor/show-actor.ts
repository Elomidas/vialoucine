import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActorsProvider } from "../../providers/actors/actors";
import { Actor } from "../../objects/actor";
import { Film } from "../../objects/film";

/**
 * Generated class for the ShowActorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-actor',
  templateUrl: 'show-actor.html',
})
export class ShowActorPage {
  actor: Actor;
  films: Film[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private actorsProvider: ActorsProvider
  ) {
    this.actor = navParams.get('actor');
    this.actorsProvider.getActorFilms(this.actor)
      .subscribe(data => {
      this.films = <Film[]>data;

    });
  }

}
