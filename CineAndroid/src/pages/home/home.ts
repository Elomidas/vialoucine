import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ActorsProvider} from "../../providers/actors/actors";
import {FilmsProvider} from "../../providers/films/films";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  changeIP: string;
  usedIP: string;

  constructor(public navCtrl: NavController) {
    this.changeIP = "192.168.1.38";
    this.usedIP = this.changeIP;
  }

  setIP() {
    ActorsProvider.setIP(this.changeIP);
    FilmsProvider.setIP(this.changeIP);
    this.usedIP = this.changeIP;
  }

}
