import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditFilmPage } from './edit-film';

@NgModule({
  declarations: [
    EditFilmPage,
  ],
  imports: [
    IonicPageModule.forChild(EditFilmPage),
  ],
})
export class EditFilmPageModule {}
