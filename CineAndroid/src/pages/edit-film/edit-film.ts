import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Film} from "../../objects/film";
import {FilmsProvider} from "../../providers/films/films";
import {Actor} from "../../objects/actor";
import {Category} from "../../objects/category";
import {ActorsProvider} from "../../providers/actors/actors";

/**
 * Generated class for the EditFilmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-film',
  templateUrl: 'edit-film.html',
})
export class EditFilmPage {
  edit: boolean;
  film: Film;
  allActors: Actor[];
  oldActors: number[];
  actors: number[];
  allCategories: Category[];
  oldCategories: number[];
  categories: number[];
  visibleAct: boolean = false;
  visibleCat: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private filmsProvider: FilmsProvider,
    private actorsProvider: ActorsProvider
  ) {
    this.edit = navParams.get('edit');
    if(this.edit) {
      this.film = navParams.get('film');
    } else {
      this.film = new Film(0, "", "");
    }
    this.allCategories = [];
    this.oldCategories = [];
    this.categories = [];
    this.allActors = [];
    this.oldActors = [];
    this.actors = [];
  }

  ionViewWillEnter() {
    //Get categories
    this.filmsProvider.getCategories().subscribe(data => {
      this.allCategories = data;
    });
    //Get actors
    this.actorsProvider.getAllActors().subscribe(data => {
      this.allActors = data;
    });
    if(this.edit) {
      //Get current actors
      this.filmsProvider.getFilmActors(this.film.filmId).subscribe(data => {
        data.forEach(e => {
          this.actors.push(e.actorId);
        });

        this.oldActors = this.actors;
        setTimeout(() => this.visibleAct = true);
      });
      //Get current categories
      this.filmsProvider.getFilmCategories(this.film.filmId).subscribe(data => {
        data.forEach(e => {
          this.categories.push(e.categoryId);
        });

        this.oldCategories = this.categories;
        setTimeout(() => this.visibleCat = true);
      });
    }
  }

  showAlert() {
    const prompt = this.alertCtrl.create({
      title: "Are you sure you want to delete this actor ?",
      message: "",
      inputs: [],
      buttons: [
        {
          text: 'Cancel',
          handler: () => { /* Close popup */ }
        },
        {
          text: 'Yes, delete',
          handler: () => {
            this.deleteActor();
          }
        }
      ]
    });
    prompt.present();
  }

  static contains(item: number, items: number[]): boolean {
    for(const i of items) {
      if(i === item) {
        return true;
      }
    }
    return false;
  }

  private getAddedCategories(): number[] {
    let added: number[] = [];
    for(let c of this.categories) {
      if(!EditFilmPage.contains(c, this.oldCategories)) {
        added.push(c);
      }
    }
    return added;
  }

  private getDeletedCategories(): number[] {
    let deleted: number[] = [];
    for(let c of this.oldCategories) {
      if(!EditFilmPage.contains(c, this.categories)) {
        deleted.push(c);
      }
    }
    return deleted;
  }

  private getAddedActors(): number[] {
    let added: number[] = [];
    for(const a of this.actors) {
      if(!EditFilmPage.contains(a, this.oldActors)) {
        added.push(a);
      }
    }
    return added;
  }

  private getDeletedActors(): number[] {
    let deleted: number[] = [];
    for(const a of this.oldActors) {
      if(!EditFilmPage.contains(a, this.actors)) {
        deleted.push(a);
      }
    }
    return deleted;
  }

  handleActors() {
    const newActors = this.getAddedActors();
    if(newActors.length > 0) {
      console.log("add act : " + JSON.stringify(newActors));
      this.filmsProvider.addFilmActors(this.film.filmId, newActors).subscribe(
        () => {
        },
        err => {
          console.log(JSON.stringify(err))
        });
    }
    const delActors = this.getDeletedActors();
    if(delActors.length > 0) {
      for(const aid of delActors) {
        console.log("del cat : " + aid);
        this.filmsProvider.deleteFilmActor(this.film.filmId, aid).subscribe(
          () => {
          },
          err => {
            console.log(JSON.stringify(err))
          });
      }
    }
  }

  handleCategories() {
    const newCategories = this.getAddedCategories();
    if(newCategories.length > 0) {
      console.log("add cat : " + JSON.stringify(newCategories));
      this.filmsProvider.addFilmCategories(this.film.filmId, newCategories).subscribe(() => {
      });
    }
    const delCategories = this.getDeletedCategories();
    if(delCategories.length > 0) {
      for(const cid of delCategories) {
        console.log("del cat : " + cid);
        this.filmsProvider.deleteFilmCategory(this.film.filmId, cid).subscribe(() => {
        });
      }
    }
  }

  updateFilm() {
    this.filmsProvider.updateFilm(this.film).subscribe(() => {
      this.navCtrl.pop();
    });
    this.handleActors();
    this.handleCategories();
  }

  addFilm() {
    this.filmsProvider.addFilm(this.film).subscribe(() => {
      this.navCtrl.pop();
    });
    this.handleActors();
    this.handleCategories();
  }

  deleteActor() {
    //It's useless to delete FilmActors and FilmCategories thanks to MySQL cascade delete
    this.filmsProvider.deleteFilm(this.film).subscribe(() => {

      this.navCtrl.pop();
    });
  }

  actorChecked(actor: number): boolean {
    return EditFilmPage.contains(actor, this.actors);
  }

  categoryChecked(category: number): boolean {
    return EditFilmPage.contains(category, this.categories);
  }
}
