import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Actor } from "../../objects/actor";
import { AlertController } from 'ionic-angular';
import { ActorsProvider } from "../../providers/actors/actors";

/**
 * Generated class for the EditActorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-actor',
  templateUrl: 'edit-actor.html',
})
export class EditActorPage {

  actor: Actor;
  edit: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private actorsProvider: ActorsProvider
  ) {
    this.edit = navParams.get('edit');
    if(this.edit) {
      this.actor = navParams.get('actor');
    } else {
      this.actor = new Actor(0, "", "");
    }
  }

  showAlert() {
    const prompt = this.alertCtrl.create({
      title: "Are you sure you want to delete this actor ?",
      message: "",
      inputs: [],
      buttons: [
        {
          text: 'Cancel',
          handler: () => { /* Close popup */ }
        },
        {
          text: 'Yes, delete',
          handler: () => {
            this.deleteActor();
          }
        }
      ]
    });
    prompt.present();
  }

  updateActor() {
    this.actorsProvider.updateActor(this.actor).subscribe(() => {

      this.navCtrl.pop();
    });
  }

  addActor() {
    this.actorsProvider.addActor(this.actor).subscribe(() => {

      this.navCtrl.pop();
    });
  }

  deleteActor() {
    this.actorsProvider.deleteActor(this.actor).subscribe(() => {

      this.navCtrl.pop();
    })
  }

}
