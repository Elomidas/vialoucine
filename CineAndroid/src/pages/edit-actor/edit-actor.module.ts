import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditActorPage } from './edit-actor';

@NgModule({
  declarations: [
    EditActorPage,
  ],
  imports: [
    IonicPageModule.forChild(EditActorPage),
  ],
})
export class EditActorPageModule {}
