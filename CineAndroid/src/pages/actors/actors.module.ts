import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { ActorsPage } from './actors';
import { ActorCardComponent } from "../../components/actor-card/actor-card";
import { EditActorPage } from "../edit-actor/edit-actor";

@NgModule({
  declarations: [
    ActorsPage,
  ],
  imports: [
    IonicModule.forRoot(ActorsPage),
    IonicPageModule.forChild(ActorsPage),
    ActorCardComponent,
    EditActorPage,
  ],
})
export class ActorsPageModule {}
