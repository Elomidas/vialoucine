import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController} from 'ionic-angular';
import { ActorsProvider } from "../../providers/actors/actors";
import { Actor } from "../../objects/actor";
import { EditActorPage } from "../edit-actor/edit-actor";

/**
 * Generated class for the ActorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actors',
  templateUrl: 'actors.html',
})
export class ActorsPage {
  actors: Actor[];
  filtered: Actor[];
  search: string;

  constructor(
    public navCtrl: NavController,
    private actorsProvider: ActorsProvider,
    public loadingCtrl: LoadingController) {
    this.actors = [];
    this.search = "";
  }

  refresh(refresher) {
    let loading;
    if(refresher == null) {
      loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();
    }

    this.actors = [];
    this.filtered = [];
    this.search = "";

    this.actorsProvider.getAllActors()
      .subscribe(data => {
        this.actors = <Actor[]>data;
        this.filtered = this.actors;

        if(refresher != null) {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        } else loading.dismiss();
      });
  }

  ionViewWillEnter() {
      this.refresh(null);
  }

  filterActors(){
    const terms = this.search.toLowerCase().split(' ');
    return this.actors.filter((actor) => {
      let test = false;
      for (let i = 0; (i < terms.length) && !test; i++) {
        if(!test && terms[i] !== "") {
          test = actor.firstName.toLowerCase().includes(terms[i]);
          test = test || actor.lastName.toLowerCase().includes(terms[i]);
          if(!test) {
            const id = "" + actor.actorId;
            test = id.includes(terms[i]);
          }
        }
      }
      return test;
    });
  }

  setFilteredActors() {
    if(this.search == "") {
      this.filtered = this.actors;
    } else {
      this.filtered = this.filterActors();
    }
  }

  addActor() {
    this.navCtrl.push(EditActorPage, {edit: false});
  }

}
