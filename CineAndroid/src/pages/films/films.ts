import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';
import { FilmsProvider } from "../../providers/films/films";
import { Film } from "../../objects/film";
import {Category} from "../../objects/category";
import {EditFilmPage} from "../edit-film/edit-film";

/**
 * Generated class for the FilmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-films',
  templateUrl: 'films.html',
})
export class FilmsPage {

  films: Film[];
  categories: Category[];
  filtered: Film[];
  search: String;
  categoryFilterId: Number;
  allCat: Category;
  loading: Loading;
  visibleSearch: boolean;
  visibleFilter: boolean;
  icon: string;

  constructor(
    public navCtrl: NavController,
    private filmsProvider: FilmsProvider,
    public loadingCtrl: LoadingController
  ) {
    this.films = [];
    this.categories = [];
    this.search = "";
    this.categoryFilterId = -1;
    this.allCat = new Category(-1, "All");
    this.visibleSearch = true;
    this.visibleFilter = false;
    this.icon = "funnel";

    //Retrieve categories
    this.filmsProvider.getCategories()
      .subscribe(data => {
        this.categories.push(this.allCat);

        for(let category of <Category[]>data) {
          this.categories.push(category);
        }

      });
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Films loading',
      spinner: 'crescent'
    });
    this.loading.present();
    this.refresh(null);
  }

  refresh(refresher) {

    this.films = [];
    this.filtered = [];
    this.search = "";

    // Retrieve films

    (
      (this.categoryFilterId == this.allCat.categoryId) ?
        this.filmsProvider.getAllFilms() :
        this.filmsProvider.getFilmsByCategory(this.categoryFilterId)
    ).subscribe(data => {
      if(refresher != null) {
        setTimeout(() => {
          refresher.complete();
        }, 2000);
      } else if(this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
      }
      this.setFilms(data);
    });
  }

  setFilteredFilms() {
    if(this.search == "") {
      this.filtered = this.films;
    } else {
      this.filtered = this.filterFilms();
    }
  }

  setFilms(data) {
    this.films = <Film[]>data;
    this.filtered = this.films;

  }

  filterFilms(){
    const terms = this.search.toLowerCase().split(' ');
    return this.films.filter((film) => {
      let test = false;
      for (let i = 0; (i < terms.length) && !test; i++) {
        if(!test && terms[i] !== "") {
          test = film.title.toLowerCase().includes(terms[i]);
          test = test || film.description.toLowerCase().includes(terms[i]);
          if(!test) {
            const id = "" + film.filmId;
            test = id.includes(terms[i]);
          }
        }
      }
      return test;
    });
  }

  addFilm() {
    this.navCtrl.push(EditFilmPage, {edit: false});
  }

  toggleFilters() {
    this.icon = this.visibleFilter ? "search" : "funnel";
    this.visibleSearch = !this.visibleSearch;
    this.visibleFilter = !this.visibleFilter;
  }

}
