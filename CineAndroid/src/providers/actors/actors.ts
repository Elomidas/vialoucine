import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Actor} from "../../objects/actor";
import {Observable} from "rxjs";
import {Film} from "../../objects/film";

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActorsProvider {
  private static baseUrl: string = "http://192.168.1.38:8080/actors";

  public static setIP(ip: string) {
    ActorsProvider.baseUrl = "http://" + ip + ":8080/actors";
  }

  constructor(public http: HttpClient) {
    //Nothing here
  }

  getAllActors(): Observable<Actor[]> {
    const url = ActorsProvider.baseUrl;
    return this.http.get<Actor[]>(url);
  }

  addActor(actor): Observable<any> {
    const url = ActorsProvider.baseUrl + "/add";
    return this.http.post(url, actor);
  }

  deleteActor(actor): Observable<any> {
      const url = ActorsProvider.baseUrl + "/delete/" + actor.actorId;
      return this.http.delete(url);
  }

  updateActor(actor): Observable<any> {
    const url = ActorsProvider.baseUrl + "/update";
    return this.http.post(url, actor);
  }

  getActorFilms(actor): Observable<Film[]> {
    const url = ActorsProvider.baseUrl + "/films/" + actor.actorId;
    return this.http.get<Film[]>(url);
  }

}
