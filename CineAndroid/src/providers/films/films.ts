import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Film} from "../../objects/film";
import {Category} from "../../objects/category";
import {Observable} from "rxjs";
import {Actor} from "../../objects/actor";

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FilmsProvider {
  private static baseUrl: string = "http://192.168.1.38:8080/films";

  public static setIP(ip: string) {
    FilmsProvider.baseUrl = "http://" + ip + ":8080/films";
    console.log("new IP : " + this.baseUrl);
  }

  constructor(public http: HttpClient) {
    //Nothing here
  }

  private genericGet(url): Observable<any> {

    return this.http.get(url);
  }

  getAllFilms(): Observable<Film[]> {
    const url = FilmsProvider.baseUrl;
    return <Observable<Film[]>>this.genericGet(url);
  }

  getCategories(): Observable<Category[]> {
    const url = FilmsProvider.baseUrl + "/category";
    return <Observable<Category[]>>this.genericGet(url);
  }

  getFilmsByCategory(categoryId): Observable<Film[]> {
    const url = FilmsProvider.baseUrl + "/category/" + categoryId;
    return <Observable<Film[]>>this.genericGet(url);
  }

  getFilm(filmId): Observable<Film> {
    const url = FilmsProvider.baseUrl + "/" + filmId;
    return <Observable<Film>>this.genericGet(url);
  }

  getFilmCategories(filmId: number): Observable<Category[]> {
    const url = FilmsProvider.baseUrl + "/" + filmId + "/categories";
    return <Observable<Category[]>>this.genericGet(url);
  }

  getFilmActors(filmId: number): Observable<Actor[]> {
    const url = FilmsProvider.baseUrl + "/" + filmId + "/actors";
    return <Observable<Actor[]>>this.genericGet(url);
  }

  updateFilm(film: Film): Observable<any> {
    const url = FilmsProvider.baseUrl + "/update";

    return this.http.post(url, film);
  }

  addFilm(film: Film): Observable<any> {
    const url = FilmsProvider.baseUrl + "/add";

    return this.http.post(url, film);
  }

  addFilmCategories(filmId: number, categories: number[]) {
    const url = FilmsProvider.baseUrl + "/add/" + filmId + "/categories";

    return this.http.post(url, categories);
  }

  addFilmActors(filmId: number, actors: number[]) {
    const url = FilmsProvider.baseUrl + "/add/" + filmId + "/actors";

    return this.http.post(url, actors);
  }

  deleteFilm(film: Film): Observable<any> {
    const url = FilmsProvider.baseUrl + "/delete/" + film.filmId;

    return this.http.delete(url);
  }

  deleteFilmActor(filmId: number, actorId: number): Observable<any> {
    const url = FilmsProvider.baseUrl + "/delete/" + filmId + "/actors/" + actorId;

    return this.http.delete(url);
  }

  deleteFilmCategory(filmId: number, categoryId: number): Observable<any> {
    const url = FilmsProvider.baseUrl + "/delete/" + filmId + "/categories/" + categoryId;

    return this.http.delete(url);
  }
}
