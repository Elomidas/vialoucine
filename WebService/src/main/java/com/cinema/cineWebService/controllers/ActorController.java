package com.cinema.cineWebService.controllers;

import com.cinema.cineWebService.entities.Actor;
import com.cinema.cineWebService.entities.Film;
import com.cinema.cineWebService.entities.FilmActor;
import com.cinema.cineWebService.repositories.ActorRepository;
import com.cinema.cineWebService.repositories.FilmActorRepository;
import com.cinema.cineWebService.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/actors")
public class ActorController {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private FilmActorRepository filmActorRepository;

    @Autowired
    private FilmRepository filmRepository;

    @GetMapping()
    @Cacheable(value = "getActors")
    public ResponseEntity findAllActors() {
        ResponseEntity response;
        try {
            List<Actor> mesActors = actorRepository.findAll();
            response = new ResponseEntity<>(mesActors, HttpStatus.OK);
        } catch (Exception e) {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping("{id}")
    public ResponseEntity getActor(@PathVariable("id") Integer id) {
        ResponseEntity response = new ResponseEntity(HttpStatus.NOT_FOUND);
        Actor actor = actorRepository.findByActorId(id);
        if(actor != null) {
            response = new ResponseEntity<>(actor, HttpStatus.OK);
        }
        return response;
    }

    @GetMapping("films/{id}")
    public ResponseEntity getActorFilms(@PathVariable("id") Integer id){
        ResponseEntity response = new ResponseEntity(HttpStatus.NOT_FOUND);
        List<FilmActor> movieIds = filmActorRepository.findAllByFilmActorKey_ActorId(id);

        if(movieIds != null) {
            List<Film> movieList = new ArrayList<>();
            movieIds.forEach(movieId ->
                    movieList.add(filmRepository.findByFilmId(movieId.getFilmActorKey().getFilmId()))
            );
            response = new ResponseEntity<>(movieList, HttpStatus.OK);
        }

        return response;
    }

    @CacheEvict(value = "getActors", allEntries = true)
    @PostMapping("/add")
    public ResponseEntity addActor(@RequestBody Actor actor) {
        ResponseEntity response;
        try {
            actorRepository.addActor(actor.getFirstName(), actor.getLastName());
            response = new ResponseEntity(HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping("/update")
    @CacheEvict(value = "getActors", allEntries = true)
    public ResponseEntity updateActor(@RequestBody Actor actor) {
        ResponseEntity response;
        try {
            actorRepository.updateActor(actor.getActorId(), actor.getFirstName(), actor.getLastName());
            response = new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping("/delete/{id}")
    @CacheEvict(value = "getActors", allEntries = true)
    public ResponseEntity deleteActor(@PathVariable("id") Integer actorId) {
        ResponseEntity response;
        try {
            actorRepository.deleteActor(actorId);
            response = new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return response;
    }

}
