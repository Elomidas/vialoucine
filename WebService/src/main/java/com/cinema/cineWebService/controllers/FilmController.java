package com.cinema.cineWebService.controllers;

import com.cinema.cineWebService.entities.*;
import com.cinema.cineWebService.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmCategoryRepository filmCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FilmActorRepository filmActorRepository;

    @Autowired
    private ActorRepository actorRepository;


    @GetMapping()
    public ResponseEntity<List<Film>> findAllFilm() {
        List<Film> mesFilms = filmRepository.findAll();
        return new ResponseEntity<>(mesFilms, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Film> getFilm(@PathVariable("id") Integer id){
        ResponseEntity<Film> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Film film = filmRepository.findByFilmId(id);
        if(film != null) {
            response = new ResponseEntity<>(film, HttpStatus.OK);
        }
        return response;
    }

    @GetMapping("/{id}/categories")
    public ResponseEntity<List<Category>> getFilmCategories(@PathVariable("id") Integer id) {
        ResponseEntity<List<Category>> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(filmRepository.findById(id).isPresent()) {
            List<Category> categories = new ArrayList<>();
            List<FilmCategory> filmCategories = filmCategoryRepository.findAllByFilmCategoryKey_FilmId(id);
            if(filmCategories != null) {
                for(FilmCategory filmCategory : filmCategories) {
                    categoryRepository.findById(filmCategory.getFilmCategoryKey().getCategoryId()).ifPresent(categories::add);
                }
            }
            response = new ResponseEntity<>(categories, HttpStatus.OK);
        }
        return response;
    }

    @GetMapping("/{id}/actors")
    public ResponseEntity<List<Actor>> getFilmActors(@PathVariable("id") Integer id) {
        ResponseEntity<List<Actor>> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(filmRepository.findById(id).isPresent()) {
            List<Actor> actors = new ArrayList<>();
            List<FilmActor> filmActors = filmActorRepository.findAllByFilmActorKey_FilmId(id);
            if(filmActors != null) {
                for(FilmActor filmActor : filmActors) {
                    actorRepository.findById(filmActor.getFilmActorKey().getActorId()).ifPresent(actors::add);
                }
            }
            response = new ResponseEntity<>(actors, HttpStatus.OK);
        }
        return response;
    }

    @GetMapping("/category/{categ}")
    public ResponseEntity<List<Film>> getFilmByCategory(@PathVariable("categ") Integer categ) {
        ResponseEntity<List<Film>> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Category category = categoryRepository.findCategoriesByCategoryId(categ);
        if(category != null) {
            List<FilmCategory> filmCategoryList = filmCategoryRepository.findAllByFilmCategoryKey_CategoryId(category.getCategoryId());
            if(filmCategoryList != null) {
                List<Film> filmList = new ArrayList<>();

                for (FilmCategory cat : filmCategoryList) {
                    filmList.add(filmRepository.findByFilmId(cat.getFilmCategoryKey().getFilmId()));
                }

                response = new ResponseEntity<>(filmList, HttpStatus.OK);
            }
        }
        return response;
    }

    @GetMapping("/category")
    public ResponseEntity<List<Category>> getCategory() {
        List<Category> cats = categoryRepository.findAll();
        return new ResponseEntity<>(cats, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity addActor(@RequestBody Film film) {
        ResponseEntity response;
        try {
            filmRepository.save(film);
            response = new ResponseEntity(HttpStatus.CREATED);
        } catch (Exception e) {
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping("/add/{filmId}/categories")
    public ResponseEntity updateFilmCategories(@PathVariable int filmId, @RequestBody List<Integer> categories) {
        ResponseEntity response;
        try {
            if(categories != null && filmRepository.findById(filmId).isPresent()) {
                for(int cid : categories) {
                    if(categoryRepository.findById(cid).isPresent()) {
                        FilmCategory link = new FilmCategory(filmId, cid);
                        filmCategoryRepository.save(link);
                    }
                }
                response = new ResponseEntity(HttpStatus.CREATED);
            } else {
                response = new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping("/delete/{filmId}/categories/{cid}")
    public ResponseEntity deleteFilmCategories(@PathVariable int filmId, @PathVariable int cid) {
        ResponseEntity response;
        try {
            List<FilmCategory> list = filmCategoryRepository.findAllByFilmCategoryKey_FilmId(filmId);
            if(list != null) {
                for(FilmCategory link : list) {
                    if(link.getFilmCategoryKey().getCategoryId() == cid)
                    filmCategoryRepository.delete(link);
                }
            }
            response = new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping("/add/{filmId}/actors")
    public ResponseEntity addFilmActors(@PathVariable int filmId, @RequestBody List<Integer> actors) {
        ResponseEntity response;
        try {
            if(actors != null && filmRepository.findById(filmId).isPresent()) {
                for(int aid : actors) {
                    if(actorRepository.findById(aid).isPresent()) {
                        FilmActor link = new FilmActor(filmId, aid);
                        filmActorRepository.save(link);
                    }
                }
                response = new ResponseEntity(HttpStatus.CREATED);
            } else {
                response = new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping("/delete/{filmId}/actors/{aid}")
    public ResponseEntity deleteFilmActors(@PathVariable int filmId, @PathVariable int aid) {
        ResponseEntity response;
        try {
            List<FilmActor> list = filmActorRepository.findAllByFilmActorKey_FilmId(filmId);
            if(list != null) {
                for(FilmActor link : list) {
                    if(link.getFilmActorKey().getActorId() == aid)
                    filmActorRepository.delete(link);
                }
            }
            response = new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping("/update")
    public ResponseEntity updateActor(@RequestBody Film film) {
        ResponseEntity response;
        try {
            filmRepository.save(film);
            response = new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteActor(@PathVariable("id") Integer filmId) {
        ResponseEntity response;
        Film film = filmRepository.findById(filmId).orElse(null);
        if(film != null) {
            try {
                List<FilmCategory> listCat = filmCategoryRepository.findAllByFilmCategoryKey_FilmId(filmId);
                if (listCat != null) {
                    for (FilmCategory filmCategory : listCat) {
                        filmCategoryRepository.delete(filmCategory);
                    }
                }
                filmRepository.delete(film);
                response = new ResponseEntity(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                e.printStackTrace();
                response = new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        } else {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return response;
    }

}
