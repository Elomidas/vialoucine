package com.cinema.cineWebService.repositories;

import com.cinema.cineWebService.entities.FilmActor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmActorRepository extends JpaRepository<FilmActor, Integer> {

    List<FilmActor> findAllByFilmActorKey_ActorId(int actorId);
    List<FilmActor> findAllByFilmActorKey_FilmId(int filmId);

}
