package com.cinema.cineWebService.repositories;

import com.cinema.cineWebService.entities.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface FilmRepository extends JpaRepository<Film, Integer> {

    Film findByFilmId(int filmId);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO film (title, description) VALUES (:title, :description)", nativeQuery = true)
    void addFilm(@Param("title") String title,
                        @Param("description") String description);

    @Modifying
    @Transactional
    @Query(value = "UPDATE film SET title = :title, description = :description WHERE film_id = :filmId", nativeQuery = true)
    void updateFilm(@Param("filmId") int filmId,
                           @Param("title") String title,
                           @Param("description") String description);

    void deleteByFilmId(int filmId);

}
