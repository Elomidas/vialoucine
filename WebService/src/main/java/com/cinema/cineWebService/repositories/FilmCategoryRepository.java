package com.cinema.cineWebService.repositories;

import com.cinema.cineWebService.entities.FilmCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmCategoryRepository extends JpaRepository<FilmCategory, Integer> {

    List<FilmCategory> findAllByFilmCategoryKey_CategoryId(int categoryId);
    List<FilmCategory> findAllByFilmCategoryKey_FilmId(int filmId);

}
