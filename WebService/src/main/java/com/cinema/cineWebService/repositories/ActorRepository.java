package com.cinema.cineWebService.repositories;

import com.cinema.cineWebService.entities.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

    Actor findByActorId(int id);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO actor (first_name, last_name) VALUES (:firstName, :lastName)", nativeQuery = true)
    public int addActor(@Param("firstName") String firstName,
                        @Param("lastName") String lastName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE actor SET first_name = :firstName, last_name = :lastName WHERE actor_id = :actorId", nativeQuery = true)
    public int updateActor(@Param("actorId") int actorId,
                           @Param("firstName") String firstName,
                           @Param("lastName") String lastName);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM actor WHERE actor_id = :actorId", nativeQuery = true)
    public int deleteActor(@Param("actorId") int actorId);
}
