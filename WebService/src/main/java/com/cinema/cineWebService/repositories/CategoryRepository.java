package com.cinema.cineWebService.repositories;

import com.cinema.cineWebService.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Category findCategoriesByCategoryId(int categoryId);
}
