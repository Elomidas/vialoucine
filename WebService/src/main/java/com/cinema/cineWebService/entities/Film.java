package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "film")
@Getter @Setter
public class Film implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "film_id", nullable = false)
  private int filmId;

  @Column(name = "title")
  private String title;

  @Column(name = "description")
  private String description;

  @Column(name = "release_year")
  private String releaseYear;

  @Column(name = "rental_duration")
  private int rentalDuration;

  @Column(name = "rental_rate")
  private double rentalRate;

  @Column(name = "length")
  private transient int length;

  @Column(name = "replacement_cost")
  private double replacementCost;

  @Column(name = "rating")
  private String rating;

  @Column(name = "special_features")
  private String specialFeatures;

  @Column(name = "last_update")
  private java.sql.Timestamp lastUpdate;


}
