package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;


@Entity
@Table(name = "actor")
@Getter @Setter
public class Actor {

  @Id
  @Column(name = "actor_id", nullable = false)
  private int actorId;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "last_update")
  private java.sql.Timestamp lastUpdate;
}
