package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;


@Entity
@Table(name = "category")
@Getter @Setter
public class Category {

  @Id
  @Column(name = "category_id", nullable = false)
  private int categoryId;

  @Column(name = "name")
  private String name;

  @Column(name = "last_update")
  private java.sql.Timestamp lastUpdate;


}
