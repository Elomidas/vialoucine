package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;


@Entity
@Getter @Setter
@Table(name = "film_actor")
public class FilmActor {

  @EmbeddedId
  private FilmActorKey filmActorKey;

  @Column(name = "last_update")
  private java.sql.Timestamp lastUpdate;

  public FilmActor(int fid, int aid) {
    filmActorKey = new FilmActorKey(fid, aid);
    lastUpdate = Timestamp.from(Instant.now());
  }

  public FilmActor() {
    filmActorKey = null;
    lastUpdate = Timestamp.from(Instant.now());
  }


  @Embeddable
  @Getter @Setter
  public static class FilmActorKey implements Serializable {

    @Column(name = "actor_id", nullable = false)
    private int actorId;

    @Column(name = "film_id", nullable = false)
    private int filmId;

    FilmActorKey(int fid, int aid) {
      filmId = fid;
      actorId = aid;
    }

    FilmActorKey() {
      filmId = 0;
      actorId = 0;
    }
  }
}
