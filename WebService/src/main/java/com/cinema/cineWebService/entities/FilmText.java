package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;


@Entity
@Table(name = "film_text")
@Getter @Setter
public class FilmText {

  @Id
  @Column(name = "film_id", nullable = false)
  private int filmId;

  @Column(name = "title")
  private String title;

  @Column(name = "description")
  private String description;


}
