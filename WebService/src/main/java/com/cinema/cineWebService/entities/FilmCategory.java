package com.cinema.cineWebService.entities;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;


@Entity
@Getter @Setter
@Table(name = "film_category")
public class FilmCategory {

  @EmbeddedId
  private FilmCategoryKey filmCategoryKey;

  @Column(name = "last_update")
  private java.sql.Timestamp lastUpdate;

  public FilmCategory() {
    filmCategoryKey = null;
    lastUpdate = Timestamp.from(Instant.now());
  }

  public FilmCategory(int fid, int cid) {
    filmCategoryKey = new FilmCategoryKey(fid, cid);
    lastUpdate = Timestamp.from(Instant.now());
  }

  @Embeddable
  @Getter @Setter
  public static class FilmCategoryKey implements Serializable {

    @Column(name = "category_id", nullable = false)
    private int categoryId;

    @Column(name = "film_id", nullable = false)
    private int filmId;

    FilmCategoryKey(int fid, int cid) {
      filmId = fid;
      categoryId = cid;
    }

    FilmCategoryKey() {
      filmId = 0;
      categoryId = 0;
    }
  }

}
