# Cinema Project

## Intro

Cinema Project for *Web Services* course at **Polytech Lyon** - 5A INFO

## Content

  - Spring Web Service
    - Works with *MySQL* and *TomCat*
    - Dependencies handled with *Maven*
  - Angular Web Client
  - Ionic Android Client

## Spring Web Service

### Startup
Needs *MySql Server*, use *Sakila* database. Launch *MySql Server* with *docker* and import *Sakila* from file :

```bash
docker run --name ${process_name} -e MYSQL_ROOT_PASSWORD=${root_password} -p 3306:3306 -d mysql;
cd ${project_root}/db;
docker exec -i ${process_name} mysql -uroot -p${root_password} < sakila.sql;
docker exec -it ${process_name} mysql -uroot -p{root_password};
```

Now type the following lines in *SQL* shell :

```sql
CREATE USER 'userepul'@'%' IDENTIFIED BY 'epul';
GRANT ALL PRIVILEGES ON sakila.* TO 'userepul'@'%';
FLUSH PRIVILEGES;
```

Installation is now functionnal.

### Use

Just launch it.

## Angular Web Client

### Startup
To launch the *Angular* client, use npm :
```bash
cd ${project_root}/CineAngular;
# Install Angular-Cli, needed to read project informations
npm install -g @angular/cli;
npm install har-validator;
# Run the server
ng serve;
```

You now have access to the Web Client at the address [localhost:4200](http://localhost:4200).
