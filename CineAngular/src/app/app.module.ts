import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';

import { registerLocaleData } from '@angular/common';

import en from '@angular/common/locales/en';
import { NavbarComponent} from './components/navbar/navbar.component';
import { CardComponent } from './components/card/card.component';
import { FilmsComponent } from './pages/films/films.component';
import {RouterModule, Routes} from '@angular/router';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { SpinnerComponent } from './utils/spinner/spinner.component';
import { CardActorComponent } from './components/card-actor/card-actor.component';
import { ActeursComponent } from './pages/acteurs/acteurs.component';
import { AddFilmComponent } from './pages/add-film/add-film.component';
import { AddActorComponent } from './pages/add-actor/add-actor.component';
import { RechercheComponent } from './pages/recherche/recherche.component';
import { ModalFilmComponent } from './components/modal-film/modal-film.component';
import { ModalActorComponent } from './components/modal-actor/modal-actor.component';

registerLocaleData(en);

const appRoutes: Routes = [
  {
    path: '',
    component: AccueilComponent
  },
  {
    path: 'films',
    component: FilmsComponent
  },
  {
    path: 'addFilm',
    component: AddFilmComponent
  },
  {
    path: 'addActeur',
    component: AddActorComponent
  },
  {
    path: 'acteurs',
    component: ActeursComponent
  },
  {
    path: 'recherche',
    component: RechercheComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardComponent,
    FilmsComponent,
    AccueilComponent,
    SpinnerComponent,
    CardActorComponent,
    ActeursComponent,
    AddFilmComponent,
    AddActorComponent,
    RechercheComponent,
    ModalFilmComponent,
    ModalActorComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    NgZorroAntdModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
