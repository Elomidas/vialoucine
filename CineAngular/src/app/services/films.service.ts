import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Film} from '../models/film';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {any} from 'codelyzer/util/function';
import {Category} from '../models/category';
import {Cacheable} from 'ngx-cacheable';
import {Actor} from '../models/actor';

const ENDPOINT = 'http://localhost:8080/films';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private http: HttpClient) { }

  @Cacheable()
  getAllFilms(): Observable<Film[]> {
    return this.http.get<Film[]>(ENDPOINT);
  }

  addFilm(film: Film): Observable<HttpResponse<any>> {
    return this.http.post<HttpResponse<any>>(ENDPOINT + `/add`, {
      title: film.title,
      description: film.description
    }, {observe: 'response' });
  }

  deleteFilm(filmId: number): Observable<HttpResponse<any>> {
    return this.http.delete<HttpResponse<any>>(ENDPOINT + `/delete/` + filmId, {observe: 'response'});
  }

  editFilm(film: Film): Observable<HttpResponse<any>> {
    return this.http.post<HttpResponse<any>>(ENDPOINT + `/update`, {
      filmId: film.filmId,
      title: film.title,
      description: film.description
    }, {observe: 'response' });
  }

  updateFilmCategories(filmId: number, categories: number[]) {
    return this.http.post<HttpResponse<any>>(ENDPOINT + `/add/` + filmId + `/categories`,  categories);
  }

  deleteFilmCategorie(filmId: number, cid: number) {
    return this.http.delete<HttpResponse<any>>(ENDPOINT + `/delete/` + filmId + `/categories/` + cid);
  }
  getFilmCategorie(categ: number): Observable<Film[]> {
    return this.http.get<Film[]>(ENDPOINT + `/category/` + categ).pipe();
  }

  getCategorieFilm(filmId: number): Observable<Category[]> {
    return this.http.get<Category[]>(ENDPOINT + `/` + filmId + `/categories`).pipe();
  }

  getFilmActors(filmId: number): Observable<Actor[]> {
    return this.http.get<Actor[]>(ENDPOINT + `/` + filmId + `/actors`).pipe();
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(ENDPOINT + `/category`).pipe();
  }

}
