import { Injectable } from '@angular/core';
import {Film} from '../models/film';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Actor} from '../models/actor';
import { Cacheable } from 'ngx-cacheable';

const ENDPOINT = 'http://localhost:8080/actors';

@Injectable({
  providedIn: 'root'
})
export class ActeursService {

  constructor(private http: HttpClient) { }

  @Cacheable()
  getAllActors(): Observable<Actor[]> {
    return this.http.get<Actor[]>(ENDPOINT);
  }

  addActeur(acteur: Actor): Observable<HttpResponse<any>> {

    return this.http.post<HttpResponse<any>>(ENDPOINT + `/add`, {
      firstName: acteur.firstName,
      lastName: acteur.lastName
    }, {observe: 'response' });
  }

  deleteActeur(acteurId: number): Observable<HttpResponse<any>> {
    return this.http.delete<HttpResponse<any>>(ENDPOINT + `/delete/` + acteurId, {observe: 'response'});
  }


  editActeur(acteur: Actor): Observable<HttpResponse<any>> {
    return this.http.post<HttpResponse<any>>(ENDPOINT + `/update`, {
      actorId: acteur.actorId,
      firstName: acteur.firstName,
      lastName: acteur.lastName
    }, {observe: 'response' });
  }

  getFilmActeur(acteurId: number): Observable<Film[]> {
    return this.http.get<Film[]>(ENDPOINT + `/films/` + acteurId).pipe();
  }

}
