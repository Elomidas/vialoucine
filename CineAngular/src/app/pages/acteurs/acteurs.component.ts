import { Component, OnInit } from '@angular/core';
import {Film} from '../../models/film';
import { ActeursService} from '../../services/acteurs.service';
import {Actor} from '../../models/actor';

@Component({
  selector: 'app-acteurs',
  templateUrl: './acteurs.component.html',
  styleUrls: ['./acteurs.component.css']
})
export class ActeursComponent implements OnInit {

  public listeActeurs: Actor[];

  constructor(
    private acteurService: ActeursService
  ) {
    this.listeActeurs = [];
  }

  ngOnInit() {
    this.acteurService.getAllActors().subscribe(data  => {
      this.listeActeurs = data;
    });
  }


}
