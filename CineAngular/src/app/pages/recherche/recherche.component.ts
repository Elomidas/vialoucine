import { Component, OnInit } from '@angular/core';
import {Actor} from '../../models/actor';
import {ActeursService} from '../../services/acteurs.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Film} from '../../models/film';
import {FilmsService} from '../../services/films.service';
import {Category} from '../../models/category';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {

  listeActeurs: Array<Actor>;
  listeCategories: Array<Category>;
  listeFilm: Array<Film>;
  resultats: Array<String>;
  filmActeurForm: FormGroup;
  acteurFilmForm: FormGroup;
  filmCategorieForm: FormGroup;
  categorieFilmForm: FormGroup;

  constructor(
    private acteurService: ActeursService,
    private filmService: FilmsService,
    private fb: FormBuilder
  ) {
    this.listeActeurs = [];
    this.resultats = [];
    this.listeCategories = [];
    this.listeFilm = [];
  }

  ngOnInit() {
    this.acteurService.getAllActors().subscribe(data  => {
      this.listeActeurs = data;
    });

    this.filmService.getCategories().subscribe(data  => {
      this.listeCategories = data;
    });

    this.filmService.getAllFilms().subscribe(data  => {
      this.listeFilm = data;
    });

    this.filmActeurForm = this.fb.group({
      acteurChoisi  : [ null, [ Validators.required ] ]
    });
    this.filmCategorieForm = this.fb.group({
      categorieChoisi  : [ null, [ Validators.required ] ]
    });
    this.categorieFilmForm = this.fb.group({
      filmChoisi  : [ null, [ Validators.required ] ]
    });
    this.acteurFilmForm = this.fb.group({
      acteurFilmChoisi  : [ null, [ Validators.required ] ]
    });
  }

  getFilmActeurForm(): void {
    for (const i in this.filmActeurForm.controls) {
      this.filmActeurForm.controls[ i ].markAsDirty();
      this.filmActeurForm.controls[ i ].updateValueAndValidity();
    }
    this.resultats = [];
    this.acteurService.getFilmActeur(this.filmActeurForm.get('acteurChoisi').value).subscribe(data  => {
      for (const film of data) {
        this.resultats.push(film.title);
      }
    });
  }

  getFilmCategorieForm(): void {
    for (const i in this.filmCategorieForm.controls) {
      this.filmCategorieForm.controls[ i ].markAsDirty();
      this.filmCategorieForm.controls[ i ].updateValueAndValidity();
    }
    this.resultats = [];
    this.filmService.getFilmCategorie(this.filmCategorieForm.get('categorieChoisi').value).subscribe(data  => {
      for (const film of data) {
        this.resultats.push(film.title);
      }
    });
  }

  getCategorieFilmForm(): void {
    for (const i in this.categorieFilmForm.controls) {
      this.categorieFilmForm.controls[ i ].markAsDirty();
      this.categorieFilmForm.controls[ i ].updateValueAndValidity();
    }
    this.resultats = [];
    this.filmService.getCategorieFilm(this.categorieFilmForm.get('filmChoisi').value).subscribe(data  => {
      for (const categ of data) {
        this.resultats.push(categ.name);
      }
    });
  }

  getActeurFilmForm(): void {
    for (const i in this.acteurFilmForm.controls) {
      this.acteurFilmForm.controls[ i ].markAsDirty();
      this.acteurFilmForm.controls[ i ].updateValueAndValidity();
    }
    this.resultats = [];
    this.filmService.getFilmActors(this.acteurFilmForm.get('acteurFilmChoisi').value).subscribe(data  => {
      for (const actor of data) {
        this.resultats.push(actor.firstName + ' ' + actor.lastName);
      }
    });
  }
}
