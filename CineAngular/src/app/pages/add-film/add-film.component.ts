import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Film} from '../../models/film';
import {FilmsService} from '../../services/films.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {NzMessageService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-film',
  templateUrl: './add-film.component.html',
  styleUrls: ['./add-film.component.css']
})
export class AddFilmComponent implements OnInit {

  validateForm: FormGroup;
  film: Film;

  constructor(
    private fb: FormBuilder,
    private filmService: FilmsService,
    public messageService: NzMessageService,
    private router: Router,
  ) {
    this.film = new Film(0, '','');
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty();
      this.validateForm.controls[ i ].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      this.film.title = this.validateForm.get('title').value;
      this.film.description = this.validateForm.get('description').value;
      this.filmService.addFilm(this.film).subscribe((response: HttpResponse<any>) => {
          if (response.status === 201) {
            this.messageService.create('success', 'Le film a été ajouté !');
            location.reload();
            this.router.navigate(['films']);
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409 || error.status === 400) {
            this.messageService.create('error', 'Le film n\'a pas pu être ajouté !');
          }
          throw error;
        });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      title: [ null, [ Validators.required ] ],
      description: [ null, [ Validators.required ] ]
    });
  }

}
