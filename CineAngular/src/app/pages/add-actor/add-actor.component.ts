import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Actor} from '../../models/actor';
import {ActeursService} from '../../services/acteurs.service';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-actor',
  templateUrl: './add-actor.component.html',
  styleUrls: ['./add-actor.component.css']
})
export class AddActorComponent implements OnInit {

  validateForm: FormGroup;
  acteur: Actor;

  constructor(
    private fb: FormBuilder,
    private acteurService: ActeursService,
    public messageService: NzMessageService,
    private router: Router,
  ) {
    this.acteur = new Actor(0, '','');
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty();
      this.validateForm.controls[ i ].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      this.acteur.lastName = this.validateForm.get('lastName').value;
      this.acteur.firstName = this.validateForm.get('firstName').value;
      this.acteurService.addActeur(this.acteur).subscribe((response: HttpResponse<any>) => {
          if (response.status === 201) {
            this.messageService.create('success', 'L\' acteur a été ajouté !');
            location.reload();
            this.router.navigate(['acteurs']);

          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409 || error.status === 400) {
            this.messageService.create('error', 'L\' acteur n\'a pas pu être ajouté !');
          }
          throw error;
        });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      lastName: [ null, [ Validators.required ] ],
      firstName: [ null, [ Validators.required ] ]
    });
  }

}
