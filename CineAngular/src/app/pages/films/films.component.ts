import { Component, OnInit } from '@angular/core';
import { FilmsService} from '../../services/films.service';
import {Observable} from 'rxjs';
import {Film} from '../../models/film';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  public listeFilms: Film[];

  constructor(
    private filmsService: FilmsService
  ) {
    this.listeFilms = [];
  }

  ngOnInit() {
    this.filmsService.getAllFilms().subscribe(data  => {
      this.listeFilms = data;
    });
  }


}
