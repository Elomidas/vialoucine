import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Actor} from '../../models/actor';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActeursService} from '../../services/acteurs.service';
import {NzMessageService} from 'ng-zorro-antd';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FilmsService} from '../../services/films.service';

@Component({
  selector: 'app-modal-actor',
  templateUrl: './modal-actor.component.html',
  styleUrls: ['./modal-actor.component.css']
})
export class ModalActorComponent implements OnInit {
  @ViewChild('content') content: any;
  closeResult: string;

  @Input()
  acteur: Actor;

  @Input()
  isVisible: boolean;

  @Output()
  change: EventEmitter<boolean> = new EventEmitter<boolean>();
  validateForm: FormGroup;

  constructor(
    private acteurService: ActeursService,
    private filmService: FilmsService,
    public messageService: NzMessageService,
    private fb: FormBuilder,
    private modal: NgbModal
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      lastName: [ this.acteur.lastName, [ Validators.required] ],
      firstName: [ this.acteur.firstName, [Validators.required] ],
    });
  }

  submitEditForm(): void {

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty()
      this.validateForm.controls[ i ].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      this.acteur.firstName = this.validateForm.get('firstName').value;
      this.acteur.lastName = this.validateForm.get('lastName').value;
      this.acteurService.editActeur(this.acteur).subscribe((response: HttpResponse<any>) => {
          if (response.status === 200) {
            this.messageService.create('success', 'L\'acteur a été modifié !');
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409 || error.status === 400) {
            this.messageService.create('error', 'L\'acteur n\'a pas pu être modifié !');
          }
          throw error;
        });
      this.isVisible = false;
    }
  }

  openEditActor() {
    this.isVisible = true;
    this.modal.open(this).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log(reason);
    });
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
