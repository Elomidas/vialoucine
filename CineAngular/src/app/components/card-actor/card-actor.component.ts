import {Component, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Actor} from '../../models/actor';
import {ActeursService} from '../../services/acteurs.service';
import {EventEmitter} from 'events';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {NzMessageService} from 'ng-zorro-antd';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalFilmComponent} from '../modal-film/modal-film.component';
import {ModalActorComponent} from '../modal-actor/modal-actor.component';


@Component({
  selector: 'app-card-actor',
  templateUrl: './card-actor.component.html',
  styleUrls: ['./card-actor.component.css']
})
export class CardActorComponent implements OnInit {

  @ViewChild('modalFilm') modal: ModalActorComponent;

  @Input()
  acteur: Actor;
  isVisible = false;

  constructor(
    private acteurService: ActeursService,
    public messageService: NzMessageService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {

  }

  deleteActeur(): void {
    this.acteurService.deleteActeur(this.acteur.actorId).subscribe((response: HttpResponse<any>) => {
      console.log(response.status);
        if (response.status === 204) {
          this.messageService.create('success', 'L\'acteur a été supprimé!');
          location.reload();
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 409 || error.status === 400) {
          this.messageService.create('error', 'L\'acteur n\'a pas pû être supprimé !');
        }
        throw error;
      });
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  editActeur(): void {
    this.isVisible = true;
    this.modal.openEditActor();
  }

  notVisible(event) {
    this.isVisible = event;
  }
}
