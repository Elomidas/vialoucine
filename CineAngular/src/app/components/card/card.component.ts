import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Film} from '../../models/film';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FilmsService} from '../../services/films.service';
import {NzMessageService} from 'ng-zorro-antd';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalFilmComponent} from '../modal-film/modal-film.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @ViewChild('modalFilm') modal: ModalFilmComponent;
  isVisible: boolean;

  constructor(
    private fb: FormBuilder,
    private filmService: FilmsService,
    public messageService: NzMessageService,
  ) { }

  @Input()
  film: Film;



  ngOnInit() {
    this.isVisible = false;
  }

  editFilm(): void {
    this.isVisible = true;
    this.modal.openEditFilm();
  }

  deleteFilm(): void {
    this.filmService.deleteFilm(this.film.filmId).subscribe((response: HttpResponse<any>) => {
        if (response.status === 204) {
          this.messageService.create('success', 'Le film a été supprimé');
          location.reload();
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 409 || error.status === 400) {
          this.messageService.create('error', 'Le film n\'a pas pû être supprimé !');
        }
        throw error;
      });
  }

  notVisible(event) {
    this.isVisible = event;
  }

}
