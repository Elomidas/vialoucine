import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FilmsService} from '../../services/films.service';
import {NzMessageService} from 'ng-zorro-antd';
import {Film} from '../../models/film';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Category} from '../../models/category';

@Component({
  selector: 'app-modal-film',
  templateUrl: './modal-film.component.html',
  styleUrls: ['./modal-film.component.css']
})
export class ModalFilmComponent implements OnInit {

  @ViewChild('content') content: any;
  validateForm: FormGroup;
  closeResult: string;
  listeCategories: Array<Category>;
  listeChoixId = [];
  listeChoixName = [];
  listeDelete = [];
  constructor(
    private fb: FormBuilder,
    private filmService: FilmsService,
    public messageService: NzMessageService,
    private modal: NgbModal
  ) { }

  @Input()
  film: Film;

  @Input()
  isVisible: boolean;

  @Output()
  change: EventEmitter<boolean> = new EventEmitter<boolean>();

  ngOnInit() {
    this.validateForm = this.fb.group({
      title: [ this.film.title, [ Validators.required ] ],
      description: [ this.film.description, [ Validators.required ] ],
      listeChoix: [null, [] ]
    });

  }

  submitEditForm(): void {
    this.listeDelete = this.listeChoixId;
    this.listeChoixId = [];
    for (const categ of this.listeChoixName) {
      for (const c of this.listeCategories) {
        if (categ === c.name) {
          this.listeChoixId.push(c.categoryId);
        }
      }
    }

    const trueDelete = [];
    for (const c of this.listeDelete) {
      let own = false;
      for (const ctg of this.listeChoixId) {
        if (ctg === c) {
          own = true;
        }
      }
      if (!own) {
        trueDelete.push(c);
      }
    }
    console.log(this.listeChoixId);
    console.log(trueDelete);
    for (const c of trueDelete) {
      this.filmService.deleteFilmCategorie(this.film.filmId, c).subscribe();
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty()
      this.validateForm.controls[ i ].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      this.film.title = this.validateForm.get('title').value;
      this.film.description = this.validateForm.get('description').value;
      this.filmService.editFilm(this.film).subscribe((response: HttpResponse<any>) => {
          if (response.status === 200) {
            this.messageService.create('success', 'Le film a été modifié !');
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409 || error.status === 400) {
            this.messageService.create('error', 'Le film n\'a pas pu être modifié !');
          }
          throw error;
        });

      this.filmService.updateFilmCategories(this.film.filmId, this.listeChoixId).subscribe();

      this.isVisible = false;
      this.change.emit(this.isVisible);
    }
  }

  openEditFilm() {
    this.listeChoixName = [];
    this.filmService.getCategories().subscribe(data  => {
      this.listeCategories = data;
    });
    this.filmService.getCategorieFilm(this.film.filmId).subscribe(data  => {
      for (const d of data) {
        this.listeChoixId.push(d.categoryId);
        this.listeChoixName.push(d.name);
      }
    });


    this.isVisible = true;
    this.modal.open(this).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log(reason);
    });
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
