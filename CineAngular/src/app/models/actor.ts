export class Actor {

  actorId: number;
  firstName: string;
  lastName: string;

  constructor(actorId: number, firstName: string, lastName: string) {
    this.actorId = actorId;
    this.firstName = firstName;
    this.lastName = lastName;
  }

}
